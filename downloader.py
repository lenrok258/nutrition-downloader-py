import csv
import sys

import requests

__PRINT_ORDER = ["Energy (kcal)",
                 "Protein (g)",
                 "Total lipid (fat) (g)",
                 "Sugars, total (g)",
                 "Carbohydrate, by difference (g)",
                 "Fiber, total dietary (g)",
                 "Zinc, Zn (mg)",
                 "Phosphorus, P (mg)",
                 "Iodine, I (µg)",
                 "Magnesium, Mg (mg)",
                 "Copper, Cu (mg)",
                 "Potassium, K (mg)",
                 "Selenium, Se (µg)",
                 "Sodium, Na (mg)",
                 "Calcium, Ca (mg)",
                 "Iron, Fe (mg)",
                 "Vitamin A, RAE (µg)",
                 "Vitamin C, total ascorbic acid (mg)",
                 "Vitamin D (D2 + D3) (µg)",
                 "Vitamin E (alpha-tocopherol) (mg)",
                 "Vitamin B-12 (µg)",
                 "Vitamin B-6 (mg)",
                 "Vitamin K (phylloquinone) (µg)",
                 "Folic acid (µg)",
                 "Water (g)"
                 ]
__URL_TEMPLATE = "https://ndb.nal.usda.gov/ndb/foods/show/{}?format=Abridged&reportfmt=csv&Qv=1"

results_map = {}


def get_url():
    report_number = sys.argv[1]
    url = __URL_TEMPLATE.format(report_number)
    return url


def get_column_to_read():
    # elvis, is that you?
    return int(sys.argv[2]) if len(sys.argv) > 2 else 1


def get_value_from_results(key):
    value = results_map.get(key, '???')
    return value.replace('.', ',')


def compose_key(row):
    return "{} ({})".format(row[0], row[1])


print()
print("URL={}".format(get_url()))
print("Column number={}".format(get_column_to_read()))
print()

response = requests.get(get_url())
response_text = response.text

for line in response_text.split("\n"):
    line_to_parse = [line]
    csv_reader = csv.reader(line_to_parse, delimiter=',', quotechar='"')
    for row in csv_reader:
        if len(row) > 2:
            results_map[compose_key(row)] = row[get_column_to_read() + 1]

for key in __PRINT_ORDER:
    print("{} = {}".format(key, get_value_from_results(key)))

print()

for key in __PRINT_ORDER:
    print(get_value_from_results(key), end='' + "\t")

print()
