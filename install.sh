#!/usr/bin/env bash

pip install virtualenv

virtualenv -p python3 .env

./.env/bin/pip install -r pip-dep.txt
